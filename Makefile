# Makefile to build the pkgdown website of spam
# can be used for spam-devel and spam


all: pkgdown jss

pkgdown:
	Rscript -e "pkgdown::build_site()"

jss:
	cp -p vignettes/abn_v2.?.pdf docs/articles/


clean:
	rm -rf docs/

